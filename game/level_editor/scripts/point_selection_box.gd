signal position_changed(id, pos)
signal double_clicked(id)
signal point_deleted(id)
extends "res://level_editor/scripts/editor_selection_box.gd"

var id = -1
var last_click = 0

func _enter_tree():
	target = self
	connect("moved", self, "_on_moved")

func _on_moved():
	emit_signal("position_changed", id, rect_position)

func _gui_input(event):
	if event.is_action_pressed("editor_place"):
		if OS.get_ticks_msec() - last_click < 200:
			emit_signal("double_clicked", id)
			last_click = OS.get_ticks_msec() - 201
		else:
			last_click = OS.get_ticks_msec()
	
	if event.is_action_pressed("editor_delete"):
		emit_signal("point_deleted", id)