signal moved
extends Control

export (bool) var ROTATABLE = true

var target
var grabbed = false
var offset = Vector2()
var rotation_dir = 0

func _ready():
	if !target:
		target = owner
	
	connect("mouse_entered", self, "_on_mouse_entered")
	connect("mouse_exited", self, "_on_mouse_exited")
	focus_mode = Control.FOCUS_ALL
	mouse_default_cursor_shape = Control.CURSOR_DRAG

func _draw():
	if has_focus():
		draw_rect(Rect2(Vector2(), rect_size), Color(1, 0, 0), false)

func _on_mouse_entered():
	grab_focus()
	update()

func _gui_input(event):
	if event.is_action("editor_place"):
		if event.is_action_pressed("editor_place"):
			if target.get("global_position"):
				offset = target.global_position - get_global_mouse_position()
			elif target.get("rect_global_position"):
				offset = target.rect_global_position - get_global_mouse_position()
		grabbed = Input.is_action_pressed("editor_place")
	
	if event.is_action_pressed("editor_delete"):
		if target != self:
			target.queue_free()
	
	if event.is_action("editor_rotate_right") or event.is_action("editor_rotate_left"):
		rotation_dir = int(Input.is_action_pressed("editor_rotate_right")) - int(Input.is_action_pressed("editor_rotate_left"))

func _process(delta):
	if grabbed:
		if has_focus():
			var grid_size = 1
			if get_tree().has_meta("grid_size"):
				grid_size = get_tree().get_meta("grid_size")
			if target.get("global_position") != null:
				target.global_position = (get_global_mouse_position() + offset).snapped(Vector2(grid_size, grid_size))
			elif target.get("rect_global_position") != null:
				target.rect_global_position = (get_global_mouse_position() + offset).snapped(Vector2(grid_size, grid_size))
			
			if ROTATABLE and rotation_dir != 0:
				if target.get("rotation_degrees") != null:
					target.rotation_degrees += rotation_dir * 720 * delta
				elif target.get("rect_rotation") != null:
					target.rect_rotation += rotation_dir * 720 * delta
	
			emit_signal("moved")
		else:
			grabbed = false
			_on_mouse_exited()

func _on_mouse_exited():
	if !grabbed:
		release_focus()
		update()
