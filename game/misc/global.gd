extends Node

const PASSWORD = "imKv3ajw"

var display_fps = false
var current_level
var dev_mode

func _ready():
	var file = File.new()
	dev_mode = file.file_exists("res://editor_secret.scrt")
	OS.window_borderless = false
	load_options()

func get_level_header(path):
	var level_file = File.new()
	level_file.open_encrypted_with_pass(path, level_file.READ, PASSWORD)
	var level_data = parse_json(level_file.get_as_text())
	var level_header = level_data[0]
	level_file.close()
	
	return level_header

func load_level(path):
	var level_file = File.new()
	level_file.open_encrypted_with_pass(path, level_file.READ, PASSWORD)
	var level_data = parse_json(level_file.get_as_text())
	var level_header = level_data[0]
	var level_bytes = level_data[1]
	
	var temp_path = "user://user_level_temp.tscn"
	var temp_file = File.new()
	temp_file.open(temp_path, temp_file.WRITE)
	temp_file.store_string(level_bytes)
	temp_file.close()
	
	var level = load(temp_path)
	
	var dir = Directory.new()
	dir.remove(temp_path)
	
	return level

func save_level(level, save_path):
	var save_dir = Directory.new()
	if !save_dir.dir_exists(save_path.get_base_dir()):
		save_dir.make_dir_recursive(save_path.get_base_dir())
	
	level.set_meta("alvl_path", save_path)
	level.set_ui_visible(true)
	var cached_level = PackedScene.new()
	cached_level.pack(level)
	var temp_path = "current_level_temp.tscn"
	ResourceSaver.save(temp_path, cached_level)
	level.set_ui_visible(false)
	
	var temp_file = File.new()
	temp_file.open(temp_path, temp_file.READ)
	var level_data = temp_file.get_as_text()
	temp_file.close()
	
	var level_header = {}
	
	var level_file = File.new()
	level_file.open_encrypted_with_pass(save_path, level_file.WRITE, PASSWORD)
	level_file.store_string(to_json([level_header, level_data]))
	level_file.close()
	
	var dir = Directory.new()
	dir.remove(temp_path)

func load_options():
	var file = File.new()
	
	if file.file_exists("user://options.cfg"):
		file.open("user://options.cfg", file.READ)
		var data = parse_json(file.get_as_text())
		file.close()
		
		if data.has("resolution"):
			OS.window_size = Vector2(data.resolution[0], data.resolution[1])
		
		if data.has("fullscreen"):
			OS.window_fullscreen = data.fullscreen
		
		if data.has("display_fps"):
			display_fps = data.display_fps

func save_options():
	var option_data = {}
	option_data.resolution = [OS.window_size.x, OS.window_size.y]
	option_data.fullscreen = OS.window_fullscreen
	option_data.display_fps = display_fps
	
	var options_file = File.new()
	options_file.open("user://options.cfg", options_file.WRITE)
	options_file.store_string(to_json(option_data))
	options_file.close()
