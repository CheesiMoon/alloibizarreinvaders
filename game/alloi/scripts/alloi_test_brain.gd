extends "res://alloi/scripts/alloi_brain.gd"

enum {JUMPING, RETURNING}

var alloi
var initial_x
var furthest_x
var mode = RETURNING

func _ready():
	alloi = get_tree().get_meta("alloi")
	initial_x = alloi.position.x
	update_furthest_x()
	alloi.CONFIG.connect("updated", self, "update_furthest_x")

func update_furthest_x():
	var airtime = alloi.jump_strength * 2 / alloi.gravity
	furthest_x = initial_x + (alloi.CONFIG.move_speed * airtime)

func _process(delta):
	if !alloi:
		return
	
	match mode:
		JUMPING:
			if alloi.position.x - furthest_x > 10:
				emit_signal("move", 0)
				mode = RETURNING
				return
			
			emit_signal("move", 1)

		RETURNING:
			if alloi.position.x > initial_x:
				emit_signal("move", -1)
			else:
				emit_signal("move", 1)
			
			if abs(alloi.position.x - initial_x) < 1:
				alloi.position.x = initial_x
				mode = JUMPING
				emit_signal("jump")
