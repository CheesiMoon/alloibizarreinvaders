extends "res://alloi/scripts/alloi_brain.gd"

func _unhandled_input(event):
	if event.is_action("move_right") or event.is_action("move_left"):
		emit_signal("move", int(Input.is_action_pressed("move_right")) - int(Input.is_action_pressed("move_left")))
	
	if event.is_action_pressed("jump"):
		emit_signal("jump")
	elif event.is_action_released("jump"):
		emit_signal("stop_jump")
	
	if event.is_action_pressed("dash"):
		emit_signal("dash")
	
	if event.is_action_pressed("punch"):
		emit_signal("punch")
