extends Node

export (String) var STATE_NAME = "UNNAMED_STATE"
export (String) var ENTRANCE_ANIMATION = "UNNAMED_ANIMATION"
export (String) var PUNCH_ANIMATION = "UNNAMED_PUNCH_ANIMATION"

var animator

func _ready():
	get_parent().register_state(STATE_NAME, self)
	animator = owner.get_node("AnimationPlayer")

func enter_state():
	owner.get_node("AnimationPlayer").animation_set_next(PUNCH_ANIMATION, ENTRANCE_ANIMATION)
	
	if owner.punching:
		var frame_number = owner.get_node("AnimationPlayer").current_animation_position
		owner.get_node("AnimationPlayer").current_animation = PUNCH_ANIMATION
		owner.get_node("AnimationPlayer").seek(frame_number, true)
	else:
		owner.get_node("AnimationPlayer").play(ENTRANCE_ANIMATION)

func handle_input(event):
	if event.is_action_pressed("punch") and !owner.punching:
		owner.punching = true
		owner.get_node("AnimationPlayer").play(PUNCH_ANIMATION)

func update(delta):
	pass

func physics_update(delta):
	pass
