extends Node

var states = {}

var current_state

func _ready():
	change_state("on_ground")

func register_state(name, node):
	#print("Registering state: " + name)
	states[name] = node

func change_state(new_state):
	if states.keys().has(new_state):
		current_state = states[new_state]
		current_state.enter_state()
	else:
		print("The state machine does not have a state named: " + new_state)

func _unhandled_input(event):
	if current_state:
		current_state.handle_input(event)

func _process(delta):
	if current_state:
		current_state.update(delta)

func _physics_process(delta):
	if current_state:
		current_state.physics_update(delta)
