extends Resource

export (float) var jump_height = 10
export (float) var wall_jump_extra_height = 10
export (float) var ground_speed = 180
export (float) var air_speed = 280
export (float) var dash_length = 50
export (float) var dash_time = 0.25
export (float) var gravity = 1000
export (float) var wall_jump_away_dist = 150
export (float) var wall_slide_max_speed = 50
export (float) var max_walkable_angle = 60
