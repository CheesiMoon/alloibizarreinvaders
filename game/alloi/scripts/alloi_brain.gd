signal move(dir)
signal jump
signal stop_jump
signal punch
signal dash
extends Node

func _enter_tree():
	if get_parent().get_meta("brain"):
		get_parent().get_meta("brain").queue_free()
	
	get_parent().set_meta("brain", self)
	print("Current brain is: " + name)
